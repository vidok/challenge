require 'spec_helper'

describe Plateau do
  let(:plateau) { Plateau.new(x: size[:x], y: size[:y]) }
  describe '#initialize' do
    context 'with positive size' do
      let(:size) { { x: 3, y: 4 } }
      it 'creates new plateau' do
        expect(plateau.x).to eq(size[:x])
        expect(plateau.y).to eq(size[:y])
      end
    end

    context 'with negative size' do
      let(:size) { { x: 3, y: -4 } }
      it 'creates new plateau' do
        expect { plateau }.to raise_error(ArgumentError)
      end
    end
  end
end

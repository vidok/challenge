require 'spec_helper'

describe Rover do
  describe '#position' do
    it 'returns current position' do
      state = State.new(x: 1, y: 1, d: 'N')
      rover = Rover.new(state: '1 1 N')
      expect(rover.position).to eq('1 1 N')
    end
  end
end

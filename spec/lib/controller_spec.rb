require 'spec_helper'

describe Controller do
  describe '#initialize' do
    context 'with valid size' do
      it 'creates plateau with 5x5 size' do
        controller = Controller.new('5 5')
        expect(controller.plateau).to eq(Plateau.new(x: 5, y: 5))
      end
    end

    context 'with empty plateau size' do
      it 'raises ArgumentError' do
        expect { Controller.new('') }.to raise_error(ArgumentError)
      end
    end

    context 'with negative plateau size' do
      it 'raises ArgumentError' do
        expect { Controller.new('5 -5') }.to raise_error(ArgumentError)
      end
    end

    context 'with bad plateau size' do
      it 'raises ArgumentError' do
        expect { Controller.new('5 A 10') }.to raise_error(ArgumentError)
      end
    end

    context 'with rover command' do
      it 'parses 2 commands' do
        input = '
        5 5
        1 0 W
        MMLMMR
        '
        commands = [
          { initial_state: '1 0 W', move: 'MMLMMR' }
        ]

        controller = Controller.new(input)
        expect(controller.rovers_commands).to eq(commands)
      end
    end

    context "with rover's not complete commands" do
      it 'raises ArgumentError' do
        input = '
        5 5
        1 0 W
        '
        expect { Controller.new(input) }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#run' do
    it 'returns plateau' do
      input = '
      5 5
      1 0 W
      MMLMMR
      '
      controller = Controller.new(input)
      expect(controller.run).to be_a_kind_of(Plateau)
    end
  end

end

require 'spec_helper'

describe State do
  describe '#from_input' do
    it 'creates from string' do
      input = '1 4 N'
      expect(State.from_input(input).to_s).to eq(input)
    end
  end
end

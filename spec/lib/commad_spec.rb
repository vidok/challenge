require 'spec_helper'

describe Command do
  describe '#move' do
    let(:subject) { Command.new(command, state).invoke.to_s }
    context 'when direction is N' do
      let(:state) { State.new(x: 1, y: 2, d: 'N') }
      context 'changes "y" position' do
        let(:command) { 'M' }
        it { is_expected.to eq(State.new(x: 1, y: 3, d: 'N').to_s) }
      end

      context 'changes direction to E' do
        let(:command) { 'R' }
        it { is_expected.to eq(State.new(x: 1, y: 2, d: 'E').to_s) }
      end

      context 'changes direction to W' do
        let(:command) { 'L' }
        it { is_expected.to eq(State.new(x: 1, y: 2, d: 'W').to_s) }
      end
    end

    context 'when direction is E' do
      let(:state) { State.new(x: 1, y: 2, d: 'E') }
      context 'changes "x" position' do
        let(:command) { 'M' }
        it { is_expected.to eq(State.new(x: 2, y: 2, d: 'E').to_s) }
      end

      context 'changes direction to S' do
        let(:command) { 'R' }
        it { is_expected.to eq(State.new(x: 1, y: 2, d: 'S').to_s) }
      end

      context 'changes direction to N' do
        let(:command) { 'L' }
        it { is_expected.to eq(State.new(x: 1, y: 2, d: 'N').to_s) }
      end
    end

  end
end

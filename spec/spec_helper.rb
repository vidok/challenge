$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'controller'
require 'state'
require 'rover'
require 'plateau'
require 'command'

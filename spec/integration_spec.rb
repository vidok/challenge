require 'spec_helper'

describe 'Rovers are exploring Mars' do
  let(:input) {
    "5 5
    1 2 N
    LMLMLMLMM
    3 3 E
    MMRMMRMRRM"
  }

  let(:output) {
    "1 3 N
5 1 E"
  }

  let(:plateau) { Controller.new(input).run }

  it "has 2 rovers" do
    expect(plateau.rovers.size).to eq(2)
  end

  it "has output" do
    expect(plateau.positions.join("\n")).to eq(output)
  end
end

# Installation
```bash
bundle install
bundle exec rspec
```
# Using
```bash
cat test_input.txt | ./bin/run
```
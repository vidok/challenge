# Implemented commands control
class Controller
  attr_reader :rovers_commands

  def initialize input
    @input = input

    parse_lines
    parse_plateau_size
    parse_rovers_commands

    plateau
  end

  def run
    rovers_commands.each do |command|
      rover = Rover.new(state: command[:initial_state])
      plateau.add_rover rover
      plateau.rovers.last.exec_command! command[:move]
    end

    plateau
  end

  def plateau
    @plateau ||= Plateau.new(x: @x, y: @y)
  end

  private

  # Parse input by commands
  def parse_lines
    raise ArgumentError if @input.empty?
    @lines = @input.split("\n").map(&:strip).reject(&:empty?)
  end

  # Extract plateau size
  # create plateau command has 2 positve arguments
  def parse_plateau_size
    @x, @y = @lines.shift.split(' ')
  end

  def parse_rovers_commands
    @rovers_commands = []
    @lines.each_slice(2) do |commands|
      raise ArgumentError if commands.size < 2
      @rovers_commands.push(
        { initial_state: commands.shift, move: commands.shift }
      )
    end
  end
end

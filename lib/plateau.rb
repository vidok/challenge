class Plateau
  attr_reader :x, :y, :rovers

  def initialize(x:, y:)
    @x = x.to_i
    @y = y.to_i
    @rovers = []

    validate_size
  end

  def ==(other)
    x == other.x && y == other.y
  end

  def add_rover rover
    @rovers << rover
  end

  def positions
    rovers.map(&:position)
  end

  private

  def validate_size
    raise ArgumentError if @x <= 0 || @y <= 0
  end
end

class Rover
  def initialize(state:)
    @state = State.from_input state
  end

  def exec_command! command
    command.chars.each do |char|
      @state = Command.new(char, @state).invoke
    end
  end

  def position
    "#{@state.x} #{@state.y} #{@state.d}"
  end
end

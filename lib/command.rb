class Command
  MOVE = 'M'.freeze
  LEFT = 'L'.freeze
  RIGHT = 'R'.freeze

  def initialize command, state
    @command = command
    @state = state
  end

  def invoke
    case @command
    when MOVE
      move
    when LEFT, RIGHT
      rotate
    else
      raise ArgumentError, 'unknown command'
    end
  end

  private

  def move
    delta  = calculate_delta
    State.new(
      x: @state.x + delta[:x],
      y: @state.y + delta[:y],
      d: @state.d
   )
  end

  def calculate_delta
    case @state.d
    when 'N'
     {x: 0, y: 1}
    when 'E'
     {x: 1, y: 0}
    when 'S'
     {x: 0, y: -1}
    when 'W'
     {x: -1, y: 0}
    end
  end

  def rotate
    State.new(
      x: @state.x,
      y: @state.y,
      d: calculate_direction(@command)
    )
  end

  def calculate_direction cmd
    current_index = State::DIRECTIONS.index(@state.d)
    return 0 unless current_index
    direction =
      if cmd == LEFT
        current_index - 1
      elsif cmd == RIGHT
        current_index + 1
      else
        current_index
      end

    State::DIRECTIONS[direction % 4]
  end
end

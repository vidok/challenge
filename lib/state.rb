class State
  attr_accessor :x, :y, :d
  DIRECTIONS = ['N', 'E', 'S', 'W'].freeze

  def self.from_input input
    x, y, d = parse_input(input)
    new(x: x, y: y, d: d)
  end

  def self.parse_input input
    parts = input.split(' ')
    x, y, d = parts[0].to_i, parts[1].to_i, parts[2]
    raise ArgumentError if x < 0 || y < 0
    raise ArgumentError unless DIRECTIONS.include?(d)
    [x, y, d]
  end

  def initialize(x:, y:, d:)
    @x, @y, @d = x, y, d
  end

  def to_s
    "#{x} #{y} #{d}"
  end
end
